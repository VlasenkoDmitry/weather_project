# README #

Vlasenko Dmitry

Test tast Mobile Swift

You can select a city from the list or determine the location automatically when you first log in.
The Main screen includes the weather forecast now, for 24 hours, 7 days.
The List cities screen allows you to add a new city.
Loading data about cities is "parallel".
When you click on a city from the list, it will become the main one and will be displayed on the Main screen.
In order for the changes not to be saved, you need to uncomment lines 29-30 in the FirstLaunchController file. 
The installed localization is English. 
To change to Russian, you need to change the language in the scheme.
