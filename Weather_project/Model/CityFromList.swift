import Foundation

class CityFromList: Decodable {
    let id: Double?
    let name: String
    let state: String?
    let country: String?
    let coord: Coord
}


