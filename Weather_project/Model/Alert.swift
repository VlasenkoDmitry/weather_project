import Foundation
import UIKit

class Alert {
    func showAlert(firstAnswer: String = "OK",secondAnswer: String = "NO", title: String, message: String, firstAction: (() -> Void)?, secondAction: (() -> Void)?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: firstAnswer, style: .default) { _ in
            guard let action = firstAction else { return }
            action()
        }
        alert.addAction(ok)
        if firstAction != nil && secondAction != nil {
            let chooseCity = UIAlertAction(title: secondAnswer, style: .cancel) { _ in
                guard let action = secondAction else { return }
                action()
            }
            alert.addAction(chooseCity)
        }
        return alert
    }
}
