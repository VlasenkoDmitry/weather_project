import Foundation

class API {
    
    static let shared = API()
    
    let weatherURL = "https://api.openweathermap.org/data/2.5/onecall"
    

    private init() {}
    
    func getInfo<T: Decodable>(fromURL url: String,
                               into type: T.Type,
                               withParameters parameters: [String : String],
                               completion: @escaping (T?, Error?) -> ()){
        
        guard let url = getURL(url, withParameters: parameters) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error == nil, let data = data {
                let object = JSON().decodeJSON(data: data, model: type)
                completion(object, error)
            } else {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    private func getURL(_ url: String, withParameters parameters: [String : String]) -> URL? {
        var components = URLComponents(string: url)
        components?.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        return components?.url
    }
}



class URLParametrs {
    var urlParameters = [
        "lat": "0",
        "lon": "0",
        "exclude": "minutely,alerts",
        "appid": "81c0ddde4f4781d0525d8fdf1c1683d4",
        "units": "metric",
        "lang": "en".localize()
    ]
}
