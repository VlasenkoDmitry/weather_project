protocol SingletonDelegate: AnyObject {
    func variableDidChange()
}

import Foundation

class ListCity {
    
    static let shared = ListCity()
    private init() {}
    weak var delegate: SingletonDelegate?
    
    var listCitises = [CityFromList]()
    var changedListCitises = [CityFromList]()
    var countCitises = 0{
        didSet {
            delegate?.variableDidChange()
        }
    }
    
    func filterData(searchText: String) {
        changedListCitises = listCitises.filter({$0.name.starts(with: "\(searchText)")})
        countCitises = changedListCitises.count
    }
}
