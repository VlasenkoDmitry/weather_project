import Foundation
import RealmSwift

class RealmClass {
    
    static let shared = RealmClass()
    
    private init() {}
    
    var realm = try! Realm()
    
    func realmFile() {
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        var configuration = Realm.Configuration()
        configuration.deleteRealmIfMigrationNeeded = true
        realm = try! Realm(configuration: configuration)
    }
    
    func countRealmData() -> Int {
        var count = 0
        do {
            try realm.write({
                count = realm.objects(RealmCity.self).count
            })
        } catch let error {
            print(error)
        }
        return count
    }
    
    func checkInRealm(number: Int) -> RealmCity {
        var city = RealmCity()
        do {
            try realm.write({
                city = realm.objects(RealmCity.self)[number]
            })
        } catch let error {
            print(error)
        }
        return city
    }
    
//    func checkInRealm() -> RealmCity {
//        var city = RealmCity()
//        do {
//            try realm.write({
//                city = realm.objects(RealmCity.self)[actualNumberDataInRealm]
//            })
//        } catch let error {
//            print(error)
//        }
//        return city
//    }
    
    func addNewCity(nameCity: RealmCity) {
        do {
            try realm.write({
                realm.add(nameCity)
            })
        } catch let error {
            print(error)
        }
    }
    
    func cleanRealm() {
        try! RealmClass.shared.realm.write {
            RealmClass.shared.realm.deleteAll()
        }
    }
}
