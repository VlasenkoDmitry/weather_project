import Foundation
import RealmSwift

class RealmCity: Object {
    @objc dynamic var name: String? = nil
    @objc dynamic var lat: String? = nil
    @objc dynamic var lon: String? = nil
}
