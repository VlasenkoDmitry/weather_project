import Foundation

class JSON {
    func jsonFileAutoParse<T: Decodable>(nameFile:String, model: T.Type) -> T?{
        guard let path = Bundle.main.path(forResource: nameFile, ofType: "json") else {
            return nil }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return decodeJSON(data: data, model: model.self)
        } catch {
            print("Data err")
        }
        return nil
    }
    
    func decodeJSON<T: Decodable>(data: Data, model: T.Type) -> T?{
        let decoder = JSONDecoder()
        do {
            let result = try decoder.decode(model.self, from: data)
            return result
        } catch {
            print(error)
        }
        return nil
    }
}
