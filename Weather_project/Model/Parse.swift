import Foundation

class Parse {
    
    static let shared = Parse()
    
    func parseCityList(){
        DispatchQueue.global(qos: .default).async {
            if var array = JSON().jsonFileAutoParse(nameFile: "city.list", model: [CityFromList].self) {
                array.sort(by: { $0.name < $1.name })
                ListCity.shared.listCitises = array
                ListCity.shared.changedListCitises = array
                ListCity.shared.countCitises = array.count
            }
        }
    }
    
    
}
