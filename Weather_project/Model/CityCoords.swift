import Foundation

class Coord: Decodable {
    let lat: Double
    let lon: Double
}
