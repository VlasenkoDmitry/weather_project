import Foundation

struct Weather: Decodable {
    var name: String?
    var lat: Double?
    var lon: Double?
    let current: Current
    let hourly: [Current]
    let daily: [DailyWeather]
}

struct Current: Decodable {
    let dt: Int?
    let temp: Double?
    let humidity: Double?
    let wind_speed: Double?
    let weather: [IdWeather]
}

struct IdWeather: Decodable {
    let id: Int?
    let main: String?
    let description: String?
    let icon: String?
}

struct DailyWeather: Decodable {
    let dt: Int?
    let temp: DailyTemp
    let humidity: Double?
    let wind_speed: Double?
    let weather: [IdWeather]
}

struct DailyTemp: Decodable {
    let day: Double?
}
