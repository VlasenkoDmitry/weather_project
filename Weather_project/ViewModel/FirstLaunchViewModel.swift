protocol FLViewModelDelegate: AnyObject {
    func addCity(city: RealmCity)
}
protocol FLViewModelLocationDelegate: AnyObject {
    func alertGeolocation()
}


import Foundation
import CoreLocation

class FirstLaunchViewModel: NSObject {
    
    private var city = RealmCity()
    private let locationManager = CLLocationManager()
    weak var delegate: FLViewModelDelegate?
    weak var delegateLocation: FLViewModelLocationDelegate?
    func addNewCityToRealm() {
        RealmClass.shared.addNewCity(nameCity: city)
    }
    
    func addNewCityToRealm(city: CityFromList) {
        let newCity = RealmCity()
        newCity.name = city.name
        newCity.lat = String(city.coord.lat)
        newCity.lon = String(city.coord.lon)
        RealmClass.shared.addNewCity(nameCity: newCity)
        
    }
    
    func filterData(searchText: String) {
        ListCity.shared.filterData(searchText: searchText)
    }
    
    func cleanUserDefaults() {
        let number = 0
        UserDefaults.standard.set(number, forKey :"NumberInRealm")
    }
}

extension FirstLaunchViewModel: CLLocationManagerDelegate {
    func locationDetermination() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager : CLLocationManager, didUpdateLocations locations : [CLLocation]) {
        let location:CLLocationCoordinate2D = manager.location!.coordinate
        print("lat: \(location.latitude), lon \(location.longitude)")
        locationManager.stopUpdatingLocation()
        
        let lat = Double(location.latitude)
        let lon = Double(location.longitude)
        findCity(latitude: lat, longitude: lon, complition: { name in
            self.city.lat = String(lat)
            self.city.lon = String(lon)
            self.city.name = String(name)
            self.delegate?.addCity(city: self.city)
        })
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error: ", error)
        print("Turn on geolocation")
        delegateLocation?.alertGeolocation()
    }
    
    private func findCity(latitude: Double, longitude: Double,complition: @escaping (String)->()){
        let location = CLLocation(latitude: latitude, longitude: longitude)
        var string = "city not found".localize()
        location.fetchCityAndCountry { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            string = city + ", " + country
            complition(string)
        }
    }
}
