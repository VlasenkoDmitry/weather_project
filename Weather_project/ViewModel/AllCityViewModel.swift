import Foundation

class AllCityViewModel {
    
    var countRowselectedСities = 0
    var arrayDataCities = [Weather]()
    
    func addNewCity(city: CityFromList) {
        let newCity = RealmCity()
        newCity.name = city.name
        newCity.lat = String(city.coord.lat)
        newCity.lon = String(city.coord.lon)
        RealmClass.shared.addNewCity(nameCity: newCity)
    }
    
    func checkingAllCityInRealm(completion: @escaping (Bool) -> ()) {
        let dataCityLoadingGroup = DispatchGroup()
        let count = RealmClass.shared.countRealmData()
        var arrayCityRealm = [RealmCity]()
        var arrayLatLon = [(String,String)]()
        for i in 0..<count {
            arrayCityRealm.append(RealmClass.shared.checkInRealm(number: i))
            guard let lat = arrayCityRealm[i].lat else { return }
            guard let lon = arrayCityRealm[i].lon else { return }
            arrayLatLon.append((lat,lon))
        }
        arrayDataCities = []
        for i in 0..<count {
            dataCityLoadingGroup.enter()
            DispatchQueue.global(qos: .utility).sync {
                self.loadingDataCity(lat: arrayLatLon[i].0, lon: arrayLatLon[i].1) { result in
                    if let newCity = result {
                        self.arrayDataCities.append(newCity)
                        dataCityLoadingGroup.leave()
                    }
                }
            }
        }
        dataCityLoadingGroup.wait()
        for i in 0..<count {
            arrayDataCities[i].name = arrayCityRealm[i].name
        }
        countRowselectedСities = arrayDataCities.count
        
        if arrayDataCities.count > 0 {
            completion(true)
        } else {
            completion(false)
        }
    }
    
    func filterData(searchText: String) {
        ListCity.shared.filterData(searchText: searchText)
    }
    
    func loadingDataCity(lat: String,lon:String, completion: @escaping (Weather?) -> ()) {
        var parametrs = URLParametrs().urlParameters
        parametrs["lat"] = lat
        parametrs["lon"] = lon
        API.shared.getInfo(fromURL: API.shared.weatherURL,
                           into: Weather.self,
                           withParameters: parametrs,
                           completion: { weather, error in
            completion(weather)
        })
    }
    
    func saveActualNumberData(number: Int) {
        UserDefaults.standard.set(number, forKey :"NumberInRealm")
    }
    
    func newOpenSearchBar() {
        ListCity.shared.changedListCitises = ListCity.shared.listCitises
        ListCity.shared.countCitises = ListCity.shared.listCitises.count
    }
}
