import Foundation


class MainViewModel {
    var cityName = Bindable<String>("")
    var temperature = Bindable<String>("")
    var weather = Bindable<String>("")
    private var city = RealmCity()
    var cityWeather: Weather?
    var countRowListDaysTableView = 7
    var actualNumberDataInRealm = 0
    
    func setCityName() {
        guard let name = city.name else {return}
        cityName.value = name
    }
    
    func setTemperature() {
        guard let temp = cityWeather?.current.temp else {return}
        temperature.value = String(format: "%.1f", temp) + "°C"
    }
    
    func setWeather() {
        guard let weather = cityWeather?.current.weather[0].description else {return}
        self.weather.value = weather
    }
    
    func checkingDataInRealm() {
        if let number = UserDefaults.standard.value(forKey: "NumberInRealm") as? Int {
            actualNumberDataInRealm = number
        }  else {
            actualNumberDataInRealm = 0
        }
        city = RealmClass.shared.checkInRealm(number: actualNumberDataInRealm)
    }
    
    func loadingDataCity( completion: @escaping (Bool) -> ()) {
        guard let lat = city.lat else { return }
        guard let lon = city.lon else { return }
        var parametrs = URLParametrs().urlParameters
        parametrs["lat"] = lat
        parametrs["lon"] = lon
        API.shared.getInfo(fromURL: API.shared.weatherURL,
                           into: Weather.self,
                           withParameters: parametrs,
                           completion: { weather, error in
            if let weatherNew = weather {
                self.cityWeather = weatherNew
            }
            completion(true)
        })
    }
}
