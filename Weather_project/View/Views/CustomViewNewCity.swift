import UIKit

class CustomViewNewCity: UIView {
    
    @IBOutlet weak var imageCustomView: UIImageView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    
    static func instanceFromNib(cityData: Weather) -> CustomViewNewCity{
        guard let xib = UINib(nibName: "CustomViewNewCity", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomViewNewCity else { return CustomViewNewCity()}
//        xib.imageCustomView.image = UIImage(named: "SnowDay")
        xib.imageCustomView.addGradient(colors: [UIColor.blue.cgColor, UIColor.systemTeal.cgColor, UIColor.white.cgColor])
        xib.city.text = cityData.name
        xib.weather.text = cityData.current.weather[0].description
        if let temp = cityData.current.temp {
            xib.temperature.text = String(format: "%.1f", temp) + "°C"
        }
        if let wind = cityData.current.wind_speed {
            xib.windSpeed.text = String(format: "%.1f", wind) + " m/s".localize()
        }
        xib.imageCustomView.corn()
        return xib
    }
}
