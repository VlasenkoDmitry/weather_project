import UIKit
import Kingfisher

class ListDaysTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var tempetatuteLabel: UILabel!
    
    func configure (model: MainViewModel, row: Int) {
        if row == 0 {
            dayLabel.text = "Today".localize()
        } else {
            if let dateUTC = model.cityWeather?.daily[row].dt{
                let time = NSDate(timeIntervalSince1970:TimeInterval(dateUTC))
                let dayWeek = time.dayOfWeek()
                dayLabel.text = dayWeek
            }
        }
        if let iconName = model.cityWeather?.daily[row].weather[0].icon {
            imageWeather.load(nameImage: iconName)
        }
        if let temperature = model.cityWeather?.daily[row].temp.day {
            tempetatuteLabel.text = String(String(format: "%.1f", temperature)) + "°C"
        }
        
    }    
}
