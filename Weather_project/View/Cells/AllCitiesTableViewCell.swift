import UIKit

class AllCitiesTableViewCell: UITableViewCell {
    
    func configure(nameCity: String?, country: String?, state: String?) {
        var newText: String {
            get {
                var text = ""
                let list = [nameCity,country,state]
                for i in list {
                    guard let element = i else { continue }
                    if element == ""{
                        continue
                    }
                    if text == "" {
                        text += element
                    } else {
                        text += ", " + element
                    }
                }
                return text
            }
        }
        self.textLabel?.text = "   " + newText
    }
}
