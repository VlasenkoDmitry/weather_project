import UIKit

class SelectedCitiesTableViewCell: UITableViewCell {
    func configure(cityData: Weather, height: CGFloat) {
        let customView = CustomViewNewCity.instanceFromNib(cityData: cityData)
        customView.frame = CGRect(x: 16, y: 10, width: self.frame.size.width - 32, height: height - 20)
        self.addSubview(customView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        for views in subviews{
            views.removeFromSuperview()
        }
    }
}
