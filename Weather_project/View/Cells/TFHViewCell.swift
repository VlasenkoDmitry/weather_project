import UIKit
import Kingfisher

class TFHViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var windLabel: UILabel!
    
    func configure (model: MainViewModel, row: Int) {
        if let dateUTC = model.cityWeather?.hourly[row].dt {
            let time = NSDate(timeIntervalSince1970:TimeInterval(dateUTC))
            timeLabel.text = time.date()
        }
        if let temperature = model.cityWeather?.hourly[row].temp {
            temperatureLabel.text = String(format: "%.1f", temperature) + "°C"
        }
        if let wind = model.cityWeather?.hourly[row].wind_speed {
            windLabel.text = String(String(format: "%.1f", wind)) + " m/s".localize()
        }
        if let iconName = model.cityWeather?.hourly[row].weather[0].icon {
            imageView.load(nameImage: iconName)
        }
    }
}
