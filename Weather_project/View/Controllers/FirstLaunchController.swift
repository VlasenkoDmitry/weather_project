import UIKit
import SnapKit
import MapKit

class FirstLaunchController: UIViewController {
    
    private var indicator = UIActivityIndicatorView()
    private var flModel = FirstLaunchViewModel()
    private var findCitySearchBar = UISearchBar()
    private var chooseCityTable = UITableView()
    private var buttonBack = UIButton()
    private var topBar = UIView()
    private var chooseCityLabel = UILabel()
    private var mainView = UIView()
    private var firstView = UIView()
    private var secondView = UIView()
    private var notice = UILabel()
    private let screenSaver = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Parse().parseCityList()
        RealmClass.shared.realmFile()
        initialize()
        ListCity.shared.delegate = self
        flModel.delegate = self
        flModel.delegateLocation = self
        // comment out the following 2 lines to save cities
//        RealmClass.shared.cleanRealm()
//        flModel.cleanUserDefaults()
        if RealmClass.shared.realm.isEmpty {
            firstGreeting()
        } else {
            showActivityIndicator()
            pushTabBarController()
        }
    }
    
    private func firstGreeting(){
        showActivityIndicator()
        noticeFormat()
        addAlertNewCity()
    }
    
    private func initialize() {
        screenSaver.image = UIImage(named: "Rainbow")
        screenSaver.frame = view.frame
        view.addSubview(screenSaver)
    }
    
    private func showActivityIndicator() {
        indicator = UIActivityIndicatorView(style: .large)
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.color = .black
        indicator.startAnimating()
    }
    
    private func noticeFormat() {
        notice.font = UIFont.systemFont(ofSize: 27)
        notice.alpha = 0.6
        notice.center = view.center
    }
    
    private func addAlertNewCity() {
        let firstAnswer = "Use".localize()
        let secondAnswer = "Choose city".localize()
        let title = "Hello".localize()
        let message = "Do you want to use your current location to search?".localize()
        let alert = Alert().showAlert(firstAnswer: firstAnswer, secondAnswer: secondAnswer, title: title, message: message, firstAction: firstAction, secondAction: secondAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func firstAction() {
        flModel.locationDetermination()
        view.addSubview(notice)
        notice.text = "Definition of the city".localize()
    }
    
    private func secondAction() {
        notice.text = "Loading data cities".localize()
        view.addSubview(mainView)
        mainView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(44)
            make.left.right.bottom.equalToSuperview().inset(0)
        }
        mainView.addSubview(firstView)
        firstView.snp.makeConstraints { make in
            make.top.equalTo(mainView.snp.top).inset(0)
            make.right.equalTo(mainView.snp.right).inset(0)
            make.left.equalTo(mainView.snp.left).inset(0)
            make.height.equalTo(44)
        }
        mainView.addSubview(secondView)
        secondView.snp.makeConstraints { make in
            make.left.equalTo(mainView.snp.left).inset(0)
            make.right.equalTo(mainView.snp.right).inset(0)
            make.top.equalTo(firstView.snp.bottom).inset(0)
            make.bottom.equalTo(mainView.snp.bottom).inset(0)
        }
        firstView.addSubview(topBar)
        topBar.backgroundColor = .white
        topBar.snp.makeConstraints { make in
            make.left.right.top.bottom.equalTo(firstView.snp.edges).inset(0)
        }
        topBar.addSubview(buttonBack)
        buttonBack.setTitle("Back".localize(), for: .normal)
        buttonBack.setTitleColor(.black, for: .normal)
        buttonBack.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        buttonBack.snp.makeConstraints { make in
            make.left.equalTo(topBar).inset(16)
            make.centerY.equalTo(topBar)
            make.width.equalTo(100)
        }
        topBar.addSubview(chooseCityLabel)
        chooseCityLabel.text = "Choose city".localize()
        chooseCityLabel.textColor = .black
        chooseCityLabel.textAlignment = .center
        chooseCityLabel.snp.makeConstraints { make in
            make.width.equalTo(200)
            make.centerX.centerY.equalTo(topBar)
        }
        secondView.addSubview(findCitySearchBar)
        findCitySearchBar.placeholder = "Add city".localize()
        findCitySearchBar.delegate = self
        findCitySearchBar.snp.makeConstraints { make in
            make.top.left.right.equalTo(secondView).inset(0)
        }
        secondView.addSubview(chooseCityTable)
        chooseCityTable.register(AllCitiesTableViewCell.self, forCellReuseIdentifier: "AllCitiesTableViewCell")
        chooseCityTable.dataSource = self
        chooseCityTable.delegate = self
        chooseCityTable.estimatedRowHeight = 50
        chooseCityTable.snp.makeConstraints { make in
            make.top.equalTo(findCitySearchBar.snp.bottom).inset(10)
            make.left.right.bottom.equalTo(secondView).inset(0)
        }
        if ListCity.shared.countCitises == 0{
            secondView.addSubview(notice)
            notice.snp.makeConstraints { make in
                make.center.equalTo(secondView)
            }
            showActivityIndicator()
        } else {
            chooseCityTable.reloadData()
        }
    }
    
    private func pushTabBarController(){
        guard let controler = self.storyboard?.instantiateViewController(identifier: "TabBarViewController") as? TabBarViewController else {return}
        controler.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controler, animated: true)
        }
    }
    
    @objc private func backPressed() {
        for view in view.subviews{
            if view != screenSaver {
                view.removeFromSuperview()
            }
        }
        addAlertNewCity()
    }
    
    private func hideActivityIndicator() {
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
}


extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.country, $1) }
    }
}

extension FirstLaunchController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListCity.shared.countCitises
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = chooseCityTable.dequeueReusableCell(withIdentifier: "AllCitiesTableViewCell", for: indexPath) as? AllCitiesTableViewCell else { return UITableViewCell() }
        cell.configure(nameCity: ListCity.shared.changedListCitises[indexPath.row].name, country: ListCity.shared.changedListCitises[indexPath.row].country ?? " ", state: ListCity.shared.changedListCitises[indexPath.row].state ?? " ")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        findCitySearchBar.resignFirstResponder()
        findCitySearchBar.text = nil
        for view in view.subviews{
            if view != screenSaver {
                view.removeFromSuperview()
            }
        }
        showActivityIndicator()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            self.flModel.addNewCityToRealm(city: ListCity.shared.changedListCitises[indexPath.row])
            self.pushTabBarController()
        }
    }
}

extension FirstLaunchController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if ListCity.shared.countCitises != 0 {
            filterTable(searchText: searchText)
        }
    }
    private func filterTable(searchText: String) {
        flModel.filterData(searchText: searchText)
        chooseCityTable.reloadData()
    }
}


extension FirstLaunchController: FLViewModelDelegate {
    func addCity(city: RealmCity) {
        guard let name = city.name else {return}
        addAlertСonfirmationCity(city: name)
    }
    
    private func addAlertСonfirmationCity(city:String) {
        let firstAnswer = "Yes".localize()
        let secondAnswer = "No".localize()
        let title = "Is your city".localize() + " \(city)?"
        let alert = Alert().showAlert(firstAnswer: firstAnswer, secondAnswer: secondAnswer, title: title, message: "", firstAction: firstActionСonfirmationCity, secondAction: secondActionСonfirmationCity)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            self.hideActivityIndicator()
            self.notice.removeFromSuperview()
        }
    }
    
    private func firstActionСonfirmationCity() {
        flModel.addNewCityToRealm()
        pushTabBarController()
    }
    
    private func secondActionСonfirmationCity() {
        firstGreeting()
    }
}

extension FirstLaunchController: SingletonDelegate {
    func variableDidChange() {
        DispatchQueue.main.async {
            if self.notice.window != nil {
                self.updateTableAfterParse()
            }
        }
    }
    
    private func updateTableAfterParse() {
        DispatchQueue.main.async {
            if let text = self.findCitySearchBar.text {
                self.filterTable(searchText: text)
            } else {
                self.chooseCityTable.reloadData()
            }
            self.hideActivityIndicator()
            self.notice.removeFromSuperview()
        }
    }
}

extension FirstLaunchController: FLViewModelLocationDelegate {
    func alertGeolocation() {
        let alert = Alert().showAlert(title: "Turn on geolocation".localize(), message: "", firstAction: nil, secondAction: nil)
        present(alert, animated: true, completion: nil)
    }
}
