import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var TFHCollectionView: UICollectionView!
    @IBOutlet weak var listDaysTableView: UITableView!
    @IBOutlet weak var viewTFH: UIView!
    @IBOutlet weak var listDayView: UIView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var dayForecastLabel: UILabel!
    @IBOutlet weak var topConstDayForecast: NSLayoutConstraint!
    @IBOutlet weak var topConstListDaysTableView: NSLayoutConstraint!
    @IBOutlet weak var heightConstSecondView: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTopBarLabel: UILabel!
    @IBOutlet weak var nameTFHForecastLabel: UILabel!
    private var mViewModel = MainViewModel()
    private let semaphore = DispatchSemaphore(value: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialization()
        bind()
        tabBarController?.delegate = self
        if let allCityVC = self.tabBarController?.viewControllers?.first(where: { $0 is AllCityViewController }) as? AllCityViewController {
            allCityVC.delegate = self
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.setContentOffset(CGPoint.zero, animated: true)
        mViewModel.checkingDataInRealm()
        loadingData { _ in
            self.semaphore.signal()
        }
        semaphore.wait()
        updatingDataAndViews()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nameTopBarLabel.text = "Main".localize()
        nameTFHForecastLabel.text = "HOURLY FORECAST".localize()
        dayForecastLabel.text = "7-DAY FORECAST".localize()
    }
    
    private func initialization() {
        guard let controler = self.storyboard?.instantiateViewController(identifier: "TabBarViewController") as? TabBarViewController else {return}
        heightConstSecondView.constant = topConstDayForecast.constant + topConstListDaysTableView.constant + dayForecastLabel.frame.size.height + listDaysTableView.rowHeight * CGFloat(mViewModel.countRowListDaysTableView) + controler.tabBar.frame.height
        imageView.image = UIImage(named: "Snow")
        imageView.contentMode = .scaleAspectFill
        viewTFH.corn()
        listDayView.corn()
        
    }
    
    private func bind() {
        mViewModel.cityName.bind({ text in
            self.cityName.text = text
            
        })
        mViewModel.temperature.bind({ text in
            self.temperature.text = text
            
        })
        mViewModel.weather.bind({ text in
            self.weatherLabel.text = text
            
        })
    }
    
    private func loadingData(completion: @escaping (Bool)->()) {
        DispatchQueue.global().sync {
            mViewModel.loadingDataCity() { result in
                completion(result)
            }
        }
    }
    
    private func updatingDataAndViews() {
        mViewModel.setCityName()
        mViewModel.setTemperature()
        mViewModel.setWeather()
        TFHCollectionView.reloadData()
        listDaysTableView.reloadData()
    }
    
    @IBAction func reloadPressed(_ sender: UIButton) {
        updatingDataAndViews()
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        guard let controler = self.storyboard?.instantiateViewController(identifier: "SettingsViewController") as? SettingsViewController else {return}
        //        controler.delegate = self
        navigationController?.pushViewController(controler, animated: true)
    }
}

extension MainViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TFHViewCell", for: indexPath) as? TFHViewCell else {return UICollectionViewCell()}
        cell.configure(model: mViewModel, row: indexPath.row)
        return cell
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mViewModel.countRowListDaysTableView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TDTableViewCell", for: indexPath) as? ListDaysTableViewCell else {return UITableViewCell () }
        cell.configure(model: mViewModel,row: indexPath.row )
        return cell
    }
}

extension MainViewController: AllCityViewControllerDelegate {
    func changeNumberDataInRealm(number: Int) {
        mViewModel.actualNumberDataInRealm = number
    }
}

