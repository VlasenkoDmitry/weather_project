import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var nameTopBarLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nameTopBarLabel.text = "Settings".localize()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
