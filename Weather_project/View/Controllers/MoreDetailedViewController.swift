//
//  MoreDetailedViewController.swift
//  Weather_project
//
//  Created by Ap on 10.12.21.
//

import UIKit
import SnapKit
class MoreDetailedViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var first = FactoryNewViewMoreDetailed().createView(orientation: .vertical)
        view.addSubview(first.newView)
        first.newView.frame = CGRect(x: 100, y: 100, width: 300, height: 300)
        first.firstLabel.text = "First"
        first.secondLabel.text = "Second"
    }
}

protocol NewView {
    var newView: UIView { get }
    var firstLabel: UILabel { get }
    var secondLabel: UILabel { get }
}

class VerticalView: NewView {
    
    var newView: UIView
    var firstLabel: UILabel
    var secondLabel: UILabel
    
    init() {
        newView = UIView()
        firstLabel = UILabel()
        secondLabel = UILabel()
        newView.addSubview(firstLabel)
        newView.addSubview(secondLabel)
        firstLabel.formatFirstLabelVerticalView(view: newView)
        secondLabel.formatSecondLabelVerticalView(view: newView)
    }
    
}
class HorizontalView: NewView {
    
    var newView: UIView
    var firstLabel: UILabel
    var secondLabel: UILabel
    
    init() {
        newView = UIView()
        firstLabel = UILabel()
        secondLabel = UILabel()
        newView.addSubview(firstLabel)
        newView.addSubview(secondLabel)
        newView.formatMoreDeatils()
        firstLabel.formatFirstLabelHorizontalView(view: newView)
        secondLabel.formatSecondLabelHorizontalView(view: newView)
    }

}

class FactoryNewViewMoreDetailed {
    func createView (orientation: orientationView) -> NewView{
        switch orientation {
        case .horizontal:
            return HorizontalView()
        case .vertical:
            return VerticalView()
        }
    }
}

enum orientationView {
    case horizontal
    case vertical
}


