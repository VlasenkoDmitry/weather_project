protocol AllCityViewControllerDelegate:AnyObject {
    func changeNumberDataInRealm(number:Int)
}

import UIKit
import Foundation

class AllCityViewController: UIViewController {
    
    @IBOutlet weak var selectedСitiesTableView: UITableView!
    @IBOutlet weak var citiesSearchBar: UISearchBar!
    private var findCitiesTable = UITableView()
    private var ACViewModel = AllCityViewModel()
    private var notice = UILabel()
    private var indicator = UIActivityIndicatorView()
    weak var delegate: AllCityViewControllerDelegate?
    private var backButton = UIButton()
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var nameTopBarLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        ListCity.shared.delegate = self        
        ACViewModel.checkingAllCityInRealm { result in
            if result == true {
                self.selectedСitiesTableView.reloadData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nameTopBarLabel.text = "List cities".localize()
        weatherLabel.text = "Weather".localize()
        citiesSearchBar.placeholder = "Add city".localize()
    }
    
    private func filterTable(searchText: String) {
        ACViewModel.filterData(searchText: searchText)
        findCitiesTable.reloadData()
    }
    
    private func showActivityIndicator() {
        indicator = UIActivityIndicatorView(style: .large)
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
        indicator.center = CGPoint(x: view.bounds.size.width / 2, y: view.bounds.size.height / 2)
        view.addSubview(indicator)
        indicator.color = .black
        indicator.startAnimating()
    }
    
    private func hideActivityIndicator() {
        indicator.stopAnimating()
        indicator.removeFromSuperview()
    }
    private func noticeFormat() {
        notice.text = "Loading data cities".localize()
        notice.font = UIFont.systemFont(ofSize: 27)
        notice.alpha = 0.6
    }
    
    private func updateTableAfterParse() {
        DispatchQueue.main.async {
            if let text = self.citiesSearchBar.text {
                self.filterTable(searchText: text)
            } else {
                self.findCitiesTable.reloadData()
            }
            self.hideActivityIndicator()
            self.notice.removeFromSuperview()
        }
    }
    
}

extension AllCityViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == findCitiesTable {
            self.findCitiesTable.register(AllCitiesTableViewCell.self, forCellReuseIdentifier: "AllCitiesTableViewCell")
            return ListCity.shared.countCitises
        } else {
            return ACViewModel.countRowselectedСities
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == findCitiesTable {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AllCitiesTableViewCell", for: indexPath) as? AllCitiesTableViewCell else { return UITableViewCell() }
            
            cell.configure(nameCity:ListCity.shared.changedListCitises[indexPath.row].name, country: ListCity.shared.changedListCitises[indexPath.row].country ?? " ", state: ListCity.shared.changedListCitises[indexPath.row].state ?? " ")
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCitiesTableViewCell", for: indexPath) as? SelectedCitiesTableViewCell else { return UITableViewCell() }
            cell.configure(cityData:ACViewModel.arrayDataCities[indexPath.row],height: tableView.rowHeight)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        citiesSearchBar.resignFirstResponder()
        if tableView == findCitiesTable {
            ACViewModel.addNewCity(city: ListCity.shared.changedListCitises[indexPath.row])
            findCitiesTable.removeFromSuperview()
            citiesSearchBar.text = nil
            ACViewModel.checkingAllCityInRealm { result in
                if result == true {
                    ListCity.shared.changedListCitises = ListCity.shared.listCitises
                    self.selectedСitiesTableView.reloadData()
                }
            }
        } else {
            delegate?.changeNumberDataInRealm(number: indexPath.row)
            ACViewModel.saveActualNumberData(number: indexPath.row)
            tabBarController?.selectedIndex = 0
        }
    }
}

extension AllCityViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        findCitiesTable.frame = CGRect(x: citiesSearchBar.frame.origin.x, y: citiesSearchBar.frame.origin.y + citiesSearchBar.frame.size.height, width: citiesSearchBar.frame.width, height: view.frame.size.height - citiesSearchBar.frame.origin.y - citiesSearchBar.frame.size.height)
        findCitiesTable.delegate = self
        findCitiesTable.dataSource = self
        findCitiesTable.reloadData()
        view.addSubview(findCitiesTable)
        if ListCity.shared.countCitises == 0 {
            self.showActivityIndicator()
            self.noticeFormat()
            view.addSubview(notice)
            notice.snp.makeConstraints { make in
                make.center.equalTo(view)
            }
            if ListCity.shared.countCitises != 0 {
                self.updateTableAfterParse()
            }
        } else {
            ACViewModel.newOpenSearchBar()
            findCitiesTable.reloadData()
        }
        topBar.addSubview(backButton)
        backButton.setTitle("Back".localize(), for: .normal)
        backButton.setTitleColor(.black, for: .normal)
        backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
        backButton.snp.makeConstraints { make in
            make.left.equalTo(topBar).inset(16)
            make.centerY.equalTo(topBar)
            make.width.equalTo(100)
        }
        return true
    }
    
    @objc func backPressed() {
        citiesSearchBar.resignFirstResponder()
        findCitiesTable.removeFromSuperview()
        backButton.removeFromSuperview()
        notice.removeFromSuperview()
        hideActivityIndicator()
        tabBarController?.selectedIndex = 0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if ListCity.shared.countCitises != 0 {
            filterTable(searchText: searchText)
        }
    }
}

extension AllCityViewController: SingletonDelegate {
    func variableDidChange() {
        DispatchQueue.main.async {
            if self.notice.window != nil {
                self.updateTableAfterParse()
            }
        }
    }
}
