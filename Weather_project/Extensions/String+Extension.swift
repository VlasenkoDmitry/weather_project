//
//  String+Extension.swift
//  Weather_project
//
//  Created by Ap on 8.12.21.
//

import Foundation
extension String {
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
