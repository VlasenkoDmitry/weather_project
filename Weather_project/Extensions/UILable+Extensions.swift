import Foundation
import UIKit

import Foundation
extension UILabel{
    func formatFirstLabelHorizontalView(view:UIView) {
        self.formatFrame()
        self.snp.makeConstraints { make in
            make.left.equalTo(view.snp.left).inset(16)
            make.center.equalTo(view.snp.center)
        }
    }
    func formatSecondLabelHorizontalView(view:UIView) {
        self.formatFrame()
        self.snp.makeConstraints { make in
            make.right.equalTo(view.snp.right).inset(16)
            make.center.equalTo(view.snp.center)
        }
    }
    
    func formatFirstLabelVerticalView(view:UIView) {
        self.formatFrame()
        self.snp.makeConstraints { make in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(view).inset(10)
        }
    }
    
    func formatSecondLabelVerticalView(view:UIView) {
        self.formatFrame()
        self.snp.makeConstraints { make in
            make.center.equalTo(view.snp.center)
        }
    }
    
    func formatFrame(){
        self.frame.size.width = 100
        self.frame.size.height = 40
    }
}
