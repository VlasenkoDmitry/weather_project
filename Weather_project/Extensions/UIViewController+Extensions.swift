import Foundation
import UIKit

extension UIViewController: UITabBarControllerDelegate {
    public func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let fromView = tabBarController.selectedViewController?.view, let toView = viewController.view {
            if fromView == toView {
                return false
            }
            UIView.transition(from: fromView, to: toView, duration: 0.2, options: .transitionCrossDissolve) { (finished) in
            }
        }
        return true
    }
}

