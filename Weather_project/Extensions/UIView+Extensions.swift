import Foundation
import UIKit

extension UIView{
    func corn(int: CGFloat = 20) {
        self.layer.cornerRadius = int
    }
    
    func formatMoreDeatils() {
        self.corn()
        self.backgroundColor = .gray
        self.index(ofAccessibilityElement: 0.6)
        self.frame = CGRect(x: 0, y: 10, width: 200, height: 200)
    }
}



