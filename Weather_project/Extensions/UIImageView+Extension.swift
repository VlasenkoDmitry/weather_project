import Foundation
import UIKit
import Kingfisher

extension UIImageView{
    func load(nameImage: String) {
        guard let url = URL(string:"http://openweathermap.org/img/wn/"+"\(nameImage)"+"@2x.png") else {return}
        self.kf.indicatorType = .activity
        self.kf.setImage(with:url)
    }
    
    func addGradient(colors: [CGColor]) {
        let gradient = CAGradientLayer()
        gradient.colors = colors
        gradient.opacity = 0.8
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
}
